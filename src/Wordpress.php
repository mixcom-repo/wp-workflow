<?php

namespace MixCom\Wordpress\Workflow;

/**
 * Making Wordpress life a bit more 2K19
 * 
 * @author Corné de Jong
 * @package Exed\Wordpress
 * @since 02-10-2019
 */
class Wordpress 
{
    /**
     * Classifies the wordpress global variables
     * 
     * @param string $variable      The requested global variable     
     * @param array $options        The options array
     * @return mixed                The value of the variable 
     * @throws Exception            If variable doesn't exists
     * 
     * Usage: 
     * 
     *      Wordpress::[VARIABLE_NAME](['options']);
     * 
     *      OPTIONS:
     *          - noThrow           Disables the exception  (Returns NULL by default)
     *          - ifNotValue        the value that should be returned if the variable doesn't exists
     * 
     */
    public function __callStatic(string $variable, array $options = [])
    {
        $variable = self::cleanUpVariableName($variable);

        /* Get the first passed parameter */
        /* In this case the options array */
        $options = $options[0] ?? [];

        /* Init the global var */
        global ${$method};
        /* Check if it is set */
        if(isset(${$method})) {
            /* If it does, return it */
            return ${$method};
        }

        /* Check for some options */

        /* If noThrow is passed or if there is a ifNotValue */
        if(in_array('noThrow', $options) || isset($options['ifNotValue'])) {
            /* Just return a value instead of throwing an Exception */
            return $options['ifNotValue'] ?? null;
        }

        /* If not, throw some shit around */
        throw new Exception('Global Wordpress variable "$' . $method . '" does not exists!', 1);
    }

    /**
     * Alias of __callStatic();
     * 
     * @param string $variable      The requested global variable     
     * @param array $options        The options array
     * @return mixed                The value of the variable 
     * @throws Exception            If variable doesn't exists
     */
    public static function get(string $variable, array $options = [])
    {
        return self::{$variable}($options);
    }

    public static function runAction(string $action)
    {
        do_action($action);
    }

    public static function runGenisis($actions)
    {
        if(is_string($actions)) {
            do_action('genesis_' . $actions);
            return;
        }
        
        foreach ($actions as $action) {
            do_action('genesis_' . $action);
        }
    }

    public static function runActions(array $actions)
    {
        foreach ($actions as $action) {
            do_action($action);
        }
    }

    public static function registerActions(array $actions)
    {
        foreach ($actions as $action => $handler) {
            add_action($action, $handler);
        }
    }

    public static function registerAction(string $action, $config)
    {
        if(!is_array($config)) {
            add_action($action, $config);
            return;
        }

        if(!isset($config['handler'])) {
            throw new Exception('No handler method provided for action "' . $action . '"', 1);
        }

        $arguments = [
            0 => $action,
            1 => $config['handler'],
        ];

        if(isset($config['priority'])) {
            $arguments[2] = $config['priority'];
        } else {
            $arguments[2] = null;
        }

        if(isset($config['arguments'])) {
            $arguments[3] = $config['arguments'];
        } else {
            $arguments[3] = null;
        }

        add_action(...$arguments);
    }










    /**
     * Cleans up the provided strig and formats it as the wordpress varibales
     * 
     * @param string $string        The to be cleaned string
     * @return string               The Cleanded string
     * 
     */
    protected static function cleanUpVariableName(string $string) : string
    {
        $stringArray = self::explodeCamelCase($string);

        foreach ($stringArray as $key => &$value) {
            $value = lcfirst($value);
        }
    
        return implode('_', $stringArray);
    }


    /**
     * Explodes a camelcase formated string into a word array
     * 
     * @param string $string        The camelCased stirng
     * @return array                The word array of the provided string
     * 
     */
    protected static function explodeCamelCase(string $string)
    {
        $re = '/(?#! splitCamelCase Rev:20140412)
            # Split camelCase "words". Two global alternatives. Either g1of2:
            (?<=[a-z])      # Position is after a lowercase,
            (?=[A-Z])       # and before an uppercase letter.
            | (?<=[A-Z])      # Or g2of2; Position is after uppercase,
            (?=[A-Z][a-z])  # and before upper-then-lower case.
            /x';
        return preg_split($re, $string);
    }
}
